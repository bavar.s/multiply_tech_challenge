#TO DO
# properly implement the read/write over telnet
# all of the messages written to telnet need to be properly converted to ascii 

# include some sort of end condition for telnet:
# tn.read_until('exit')

# track the stored stations/locations 
# optional: add dictionary to allow locations to be named



import telnetlib

host = '192.168.0.1'
rbot_num = 1 #update code to allow user to input
port = int(f"{10}{rbot_num}{00}") 

#attempt to connect to robot through Telnet:
try:
    tn = telnetlib.Telnet(host, port)
    print('connected to robot ' + str(rbot_num))
except:
    print('failed to connect to robot' + str(rbot_num))    

#function to teach the input position 
# x y and z can be coordinates, or 
def teachPos(x, y, z, roll, pitch, yaw):
    #write position into next empty station
    locVals = "x y z roll pitch yaw" #do a conversion with the inputs to get a string that looks like this
    message = "locXyz" + str(stationIx)+str(locVals)
    tn.write(message)

default_profile = "1" #where 1 contains some default profile 
#moves to given sation number, optional profile
def moveTo(station_num, profile):
    message = "move" + str(station_num)+str(profile)
    tn.write(message)

    #report current and goal positions, example defaults to cartesian 
    #can enhance function to allow user to choose location object type
def reportPos():
    outTarg = tn.write("wherec") #returns target location. how to listen to it?
    #could put this in a loop to report position at a desired rate
    outCurr = tn.write("wherej") 
    output = "target: " + outTarg + "current: " + outCurr
    return output

#pick from location A and places at B
    #A and B must be stations 
    #future upgrade: allow input of location. could even call teachPos
def pickPlace(posA, posB):  
    #move to A
    message = "move" + posA + default_profile
    tn.write(message)
    #perform grip 
    #move to B
    #perform release 

#other things to define:
#a default profile for motion 
#function for gripping, to be called by pickPlace
#function for releasing, to be called by pickPlace